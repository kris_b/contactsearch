// Let's create a "real-time search" component

var SearchExample = React.createClass({

    getInitialState: function(){
        return { searchString: '' };
    },

    handleChange: function(e){

        // If you comment out this line, the text box will not change its value.
        // This is because in React, an input cannot change independently of the value
        // that was assigned to it. In our case this is this.state.searchString.

        this.setState({
            searchString:e.target.value
        });
    },
    
    componentDidMount: function(){
        
        // When the component loads, send a jQuery AJAX request

        var self = this;

        var url = 'exercise-data.json';

        $.getJSON(url, function(result){

            // Update the component's state. This will trigger a render.
            // Note that this only updates the pictures property, and does
            // not remove the favorites array.
            
            var contacts = result.map(function(contact) {
                
                var searchstr = JSON.stringify(contact);

                return {
                    data: contact,
                    searchstr: searchstr
                }
            });

            self.setState({ contacts: contacts });

        });

    },


    render: function() {

        var contacts = this.state.contacts;

        if (!contacts) {
            return <div>Loading contacts...</div>
        }
        
        var searchString = this.state.searchString.trim().toLowerCase();


        if(searchString.length > 0){

            // We are searching. Filter the results.

            contacts = contacts.filter(function(contact){
                return contact.searchstr.toLowerCase().match(searchString);
            });

        }

        return <div>
                    <input type="text" value={this.state.searchString} onChange={this.handleChange} placeholder="Type here" />

                    <ul> 

                        { contacts.map(function(contact){
                            return <li>
                                <div>Name: {contact.data.name}</div>
                                <div>Company: {contact.data.company}</div>
                                <div>Email: {contact.data.email}</div>
                                <div>Country: {contact.data.country}</div>
                                <div>Job History: {contact.data.job_history.join(', ')}</div>
                            </li>
                        }) }

                    </ul>

                </div>;

    }
});
                                                                                                                                                             
// Render the SearchExample component on the page

ReactDOM.render(
    <SearchExample />,
    document.getElementById('container')
);
